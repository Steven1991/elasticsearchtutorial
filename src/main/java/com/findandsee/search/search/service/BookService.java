package com.findandsee.search.search.service;

import com.findandsee.search.search.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface BookService {

    Book save(Book book);

    void delete(Book book);

    Book findOne(String id);

    Iterable<Book> findAll();

    public Page<Book> findByAuthor(String author, PageRequest pageRequest);

    public List<Book> findByTitle(String title);

}
